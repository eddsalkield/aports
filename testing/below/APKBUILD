# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=below
pkgver=0.6.2
pkgrel=1
pkgdesc="A time traveling resource monitor for modern Linux systems"
url="https://github.com/facebookincubator/below"
# riscv64: blocked by cargo
# s390x: fails to build nix crate
arch="all !riscv64 !s390x"
license="Apache-2.0"
# NOTE: libbpf-cargo requires rustfmt
makedepends="
	cargo
	clang
	libbpf-dev
	rustfmt
	zlib-dev
	zstd-dev
	"
subpackages="
	$pkgname-openrc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/facebookincubator/below/archive/v$pkgver/$pkgname-$pkgver.tar.gz
	no-vendor.patch
	fix-libbpf-sys.patch
	fix-sudotest.patch
	Cargo.lock

	$pkgname.initd
	$pkgname.confd
	$pkgname.logrotate
	"

case "$CARCH" in
	# XXX: btrfs tests are broken
	armhf | armv7 | x86) options="!check";;
esac

_cargo_opts="--frozen --features no-vendor"

# below may not work correctly with panic=abort.
export CARGO_PROFILE_RELEASE_PANIC="unwind"

prepare() {
	default_prepare

	# https://github.com/facebookincubator/below/pull/8161
	cp "$srcdir"/Cargo.lock .

	cargo fetch --locked
}

build() {
	cargo build $_cargo_opts --release

	mkdir -p target/completion
	local sh; for sh in bash fish zsh; do
		./target/release/below generate-completions -s $sh -o target/completion/below.$sh
	done
}

check() {
	# Skip tests that require host to have cgroup2 (copied from upstream's ci.yml)
	cargo test $_cargo_opts -- \
		--skip test_dump \
		--skip advance_forward_and_reverse \
		--skip disable_disk_stat \
		--skip disable_io_stat \
		--skip record_replay_integration \
		--skip test_belowrc_to_event \
		--skip test_event_controller_override \
		--skip test_event_controller_override_failed \
		--skip test_viewrc_collapse_cgroups \
		--skip test_viewrc_default_view
}

package() {
	cd target

	install -D -m755 release/below -t "$pkgdir"/usr/bin/

	install -D -m644 completion/$pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -D -m644 completion/$pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -D -m644 completion/$pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname

	install -D -m755 "$srcdir"/below.initd "$pkgdir"/etc/init.d/$pkgname
	install -D -m644 "$srcdir"/below.confd "$pkgdir"/etc/conf.d/$pkgname
	install -D -m644 "$srcdir"/below.logrotate "$pkgdir"/etc/logrotate.d/$pkgname

	install -d -m755 "$pkgdir"/var/log/$pkgname
}

sha512sums="
d6ce555131a78a8586d3592910810aedc938d2412677d27d329993f26daa568636ea3fb0d6fe9bf0a56829089d04d87d34acc98ce9c3762e58577e8395435c9f  below-0.6.2.tar.gz
5e2057df17de6a94193c4aacd7624f2fdb7e4661251c5858bc427ec0307f64695d079930db4db6f6a3912adbdae41d84fa0827b7061fdc3721cd2278edd38d6d  no-vendor.patch
da7a59ba03dcaea674b93c54a921a5c328094309eb57e27d0ff2a8cf81203483e1c156516e02788c5e3ac54d60e082f4f125fb6114a7887d35e47c56ba8cb89b  fix-libbpf-sys.patch
1f9a380537bfab93d5b5184ffb82b0b737428c3672344b8d71ed12d18c1c798cfdaa8e5713be21551ad7e8dc1f4c6b653f1309852355daf86d45bfadd3ca37c3  fix-sudotest.patch
160be59cbfd8ad358614c92ea796a1afcd8c1f7f2ec0c9f1077c3a4093beb53896e73a3e9b79c161f209b3c73ab9e8bba2e8654ac9f213dfddce8bf12ccbc164  Cargo.lock
e15900998f592e5d519a3698aa861d77269e2196414ed69dacfbdc23a3df355b0f95cc64abc18ddcbf7b4fadafd27ee6cf6a75631d6771cf69c23cb45988c8d9  below.initd
05ca8ad81eaf6f5ccccef2e79dd9b9ec7fc296cf184128da8d99b94a6462db822cd76f42ffbecee7db009e7905c5e4bc31939fb905a80ab4faa9b10e93f9479e  below.confd
f9aa8f1d598603898396bde7404e511ccac0887e6dafd2db0b749efe255855bccb724a4969a93a29e437d344523a24859daedd8d21ad02f8fd2c70f03c6b74e5  below.logrotate
"
